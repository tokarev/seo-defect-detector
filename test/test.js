const expect = require('chai').expect;
const seoDefectsDetector = require('../index');

describe('#seoDefectsDetector', function() {
    it('empty input should return TRUE', function() {
        let result = seoDefectsDetector.run('', {allowedRules:[]});
        expect(result).to.equal(true);
    });

    it('Valid aRel rule should return TRUE', function() {
        let result = seoDefectsDetector.run('<a rel="something">', {allowedRules:['aRel']});
        expect(result).to.equal(true);
    });

    it('Invalid aRel rule should return error', function() {
        let result = seoDefectsDetector.run('<a rel="">', {allowedRules:['aRel']});
        expect(result[0]).to.equal('here are 1 <a> tag without rel atribute');
    });

    it('Valid imgAlt rule should return TRUE', function() {
        let result = seoDefectsDetector.run('<img alt="something">', {allowedRules:['imgAlt']});
        expect(result).to.equal(true);
    });

    it('Invalid imgAlt rule should return error', function() {
        let result = seoDefectsDetector.run('<img/><img/>', {allowedRules:['imgAlt']});
        expect(result[0]).to.equal('here are 2 <img> tag without alt atribute');
    });

    it('Valid h1Count rule should return TRUE', function() {
        let result = seoDefectsDetector.run('<h1></h1>', {allowedRules:['h1Count']});
        expect(result).to.equal(true);
    });

    it('Invalid h1Count rule should return error', function() {
        let result = seoDefectsDetector.run('<h1></h1><h1></h1>', {allowedRules:['h1Count']});
        expect(result[0]).to.equal('here is more than one <H1> tag');
    });

    it('Valid headMetaDescription rule should return TRUE', function() {
        let result = seoDefectsDetector.run('<head><meta name="descriptions"/><meta name="keywords"/><title></title></head>', {allowedRules:['headMetaDescription']});
        expect(result).to.equal(true);
    });

    it('Invalid headMetaDescription rule should return error', function() {
        let result = seoDefectsDetector.run('', {allowedRules:['headMetaDescription']});
        expect(result[0]).to.equal('here is no <meta name="descriptions" ... /> in <head> tag');
    });

    it('Valid headMetaKeywords rule should return TRUE', function() {
        let result = seoDefectsDetector.run('<head><meta name="descriptions"/><meta name="keywords"/><title></title></head>', {allowedRules:['headMetaKeywords']});
        expect(result).to.equal(true);
    });

    it('Invalid headMetaKeywords rule should return error', function() {
        let result = seoDefectsDetector.run('', {allowedRules:['headMetaKeywords']});
        expect(result[0]).to.equal('here is no <meta name="keywords" ... /> in <head> tag');
    });

    it('Valid headTitle rule should return TRUE', function() {
        let result = seoDefectsDetector.run('<head><meta name="descriptions"/><meta name="keywords"/><title></title></head>', {allowedRules:['headTitle']});
        expect(result).to.equal(true);
    });

    it('Invalid headTitle rule should return error', function() {
        let result = seoDefectsDetector.run('', {allowedRules:['headTitle']});
        expect(result[0]).to.equal('here is no <title> in <head> tag');
    });

    it('Valid strongCount rule should return TRUE', function() {
        let result = seoDefectsDetector.run('<strong></strong>', {allowedRules:['strongCount']});
        expect(result).to.equal(true);
    });

    it('Invalid strongCount rule should return error', function() {
        let result = seoDefectsDetector.run('<strong></strong><strong></strong><strong></strong>', {allowedRules:['strongCount'], strongCount: 2});
        expect(result[0]).to.equal('here is more than 2 <strong> tag');
    });

    it('Valid user defined rule should return TRUE', function() {
        let result = seoDefectsDetector.run(
            '<head><meta name="robots"/></head>',
            {
                allowedRules:['metaRobots'],
                userRules: {
                    metaRobots: (html) => {
                        const cheerio = require('cheerio');
                        const $ = cheerio.load(html);
                        let foundErrors = $('head meta[name="robots"]').length < 1;
                        if (foundErrors) {
                            return `here is no <meta name="robots" ... /> in <head> tag`;
                        }
                        return false;
                    }
                }
            }
        );
        expect(result).to.equal(true);
    });

    it('Invalid user defined rule should return error', function() {
        let result = seoDefectsDetector.run(
            '<head><meta name="descriptions"/></head>',
            {
                allowedRules:['metaRobots'],
                userRules: {
                    metaRobots: (html) => {
                        const cheerio = require('cheerio');
                        const $ = cheerio.load(html);
                        let foundErrors = $('head meta[name="robots"]').length < 1;
                        if (foundErrors) {
                            return `here is no <meta name="robots" ... /> in <head> tag`;
                        }
                        return false;
                    }
                }
            }
        );
        expect(result[0]).to.equal('here is no <meta name="robots" ... /> in <head> tag');
    });

    it('Result from valid HTML file should be TRUE', function() {
        let result = seoDefectsDetector.run('', {
            input: {
                type: 'file',
                filepath: 'test/fixtures/test.html'
            }
        });
        expect(result).to.equal(true);
    });

});
