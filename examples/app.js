const seoDefectsDetector = require('../index');
const fs = require('fs');

let result = seoDefectsDetector.run('<head><meta name="descriptions"/><meta name="robots"/><meta name="keywords"/><title></title></head><body><h1>ss</h1><img alt=""><a rel="23"></a></body>',
{
    allowedRules:['imgAlt'],
    userRules: {
        metaRobots: (html) => {
            const cheerio = require('cheerio');
            const $ = cheerio.load(html);
            let foundErrors = $('head meta[name="robots"]').length < 1;
            if (foundErrors) {
                return `here is no <meta name="robots" ... /> in <head> tag`;
            }
            return false;
        }
    },
    input: {
        type: 'stream',
        stream: fs.createReadStream('examples/test.html')
    },
    output: {
        type: 'stream',
        stream: fs.createWriteStream('examples/output.txt')
    }
});
