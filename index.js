const fs = require('fs');
const path = require('path');

let seoDefectsDetector = {
    rules: [],

    loadRules: (config, cb) => {
        let files = fs.readdirSync( path.join(__dirname, 'rules'));
        files.forEach(file => {
        let name = file.replace(/\.js$/, '');
            if (!config.allowedRules || (config.allowedRules && Array.isArray(config.allowedRules) && config.allowedRules.indexOf(name) != -1)) {
                seoDefectsDetector[name] = require('./rules/' + file);
                seoDefectsDetector.rules.push(name);
                if (config.allowedRules) {
                    seoDefectsDetector.rules = config.allowedRules;
                }
            }
        });
        if (config.userRules) {
            Object.keys(config.userRules).forEach((key) => {
                if (typeof config.userRules[key] == 'function') {
                    seoDefectsDetector[key] = config.userRules[key];
                    seoDefectsDetector.rules.push(key);
                }
            });
        }
        return cb();
    },

    run: (html, config = {}) => {
        let results = [];

        return seoDefectsDetector.input(html, (html) => {
            seoDefectsDetector.loadRules(config, () => {
                seoDefectsDetector.rules.forEach((rule) => {
                    let result = seoDefectsDetector[rule](html, config);
                    if (result) {
                        results.push(result);
                    }
                });
                return results.length ? results : true;
            })
            return seoDefectsDetector.output(results, config);
        }, config);
    },

    input: (originalHtml, cb, config = {}) => {
        if (config && config.input && config.input.type == 'file' && config.input.filepath) {
            return cb(fs.readFileSync(path.join(__dirname, config.input.filepath)));
        } else if (config && config.input && config.input.type == 'stream' && config.input.stream) {
            let html = '';
            config.input.stream.on('data', (chunk) => {
                html += chunk.toString('utf8');
            });
            config.input.stream.on('end', () => {
                return cb(html);
            });
        } else {
            return cb(originalHtml);
        }
    },

    output: (results, config = {}) => {
        if (config && config.output && config.output.type == 'file' && config.output.filepath) {
            if (results.length) {
                fs.writeFile(config.output.filepath, results.join("\n"), (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(`Result saved in ${config.output.filepath} file`);
                    }
                });
            }
        } else if (config && config.output && config.output.type == 'console') {
            if (results.length) {
                console.log(results.join("\n"));
            }
        } else if (config && config.output && config.output.type == 'stream' && config.output.stream) {
            return config.output.stream.write(results.join("\n"));
        } else {
            return results.length ? results : true;
        }
    }
};

module.exports = seoDefectsDetector;
