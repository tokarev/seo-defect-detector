# SEO defect detector

### Usage

Clone the git repo.

```
git clone git@bitbucket.org:tokarev/seo-defect-detector.git
```

Install the dependencies.

```
npm/yarn install
```

Run tests

```
npm/yarn test
```

### Configuration

All fields are optional

**allowedRules**  - array of SEO rules to validate HTML document. Could be both pre-defined and user defined.

Example:

```
{
    allowedRules: ['aRel', 'h1Count', 'metaRobots']
}
```

**userRules** object where you can add your own rules ( name -> function )

Example:

```javascript
{
    userRules: {
        metaRobots: (html) => {
            const cheerio = require('cheerio');
            const $ = cheerio.load(html);
            let foundErrors = $('head meta[name="robots"]').length < 1;
            if (foundErrors) {
                return `here is no <meta name="robots" ... /> in <head> tag`;
            }
            return false;
        }
    }
}
```


**input** object to set up input source of HTML

_type_ could be ```file``` or ```stream```

_filepath_ [string] mandatory for type ```file```

_stream_ [object] mandatory for type ```stream```

Example:

```javascript
{
    input: {
        type: 'stream',
        stream: fs.createReadStream('examples/test.html')
    }
}
```



**output** object to set up output for result

_type_ could be ```console```, ```file``` or ```stream```

_filepath_ [string] mandatory for type ```file```

_stream_ [object] mandatory for type ```stream```

Example:

```javascript
{
    output: {
        type: 'stream',
        stream: fs.createWriteStream('examples/output.txt')
    }
}
```




---

## ShopBack Node.js Code Challenge

### Mission

Your mission is develop a Node.js package to let user can use this package to scan a HTML file and show all of the SEO defects. [What is SEO?](https://moz.com/learn/seo/what-is-seo)

>The “User” for this package is just like you: A Node.js Developer

### Pre-defined SEO rules

You need to implement following 5 pre-defined SEO rules for this package:

1. Detect if any ```<img />``` tag without alt attribute
2. Detect if any ```<a />``` tag without rel attribute
3. In ```<head>``` tag
    * Detect if header doesn’t have ```<title>``` tag
    * Detect if header doesn’t have ```<meta name=“descriptions” ... />``` tag
    * Detect if header doesn’t have ```<meta name=“keywords” ... />``` tag
4. Detect if there’re more than 15 ```<strong>``` tag in HTML (15 is a value should be configurable by user)
5. Detect if a HTML have more than one ```<H1>``` tag.

### Development Requirement

1. This package should be production ready and a NPM module
2. User is free to chain any rules by themselves
    * For example, they can only use the rule 1 and 4 or only use rule 2.
    * The order of rules is doesn’t matter
3. User can define and use their own rules easily
4. The input can be:
    * A HTML file (User is able to config the input path)
    * Node Readable Stream
5. The output can be:
    * A file (User is able to config the output destination)
    * Node Writable Stream
    * Console
6. Your package should be flexible:
    * When we want to implement additional rules for ```<meta>``` tag, The code changes should be small. Ex: Checking ```<meta name=“robots” />``` existing or not?! 

### Example Output
Showing all of the defects for rules that user apply, following is a simple output demo when a user apply above 5 rules. (Please feel free to customize the result sentences by yourself)
```
There are 1 <img> tag without alt atribute
There are 1 <a> tag without rel atribute
This HTML without <title> tag
This HTML have more than one <h1> tag
```