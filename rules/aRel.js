// Detect if any <a /> tag without rel attribute

module.exports = function (html) {
    const cheerio = require('cheerio');
    const $ = cheerio.load(html);
    let foundErrors = $('a').length - $('a[rel!=""]').length;
    if (foundErrors) {
        return `here are ${foundErrors} <a> tag without rel atribute`;
    }
    return false;
}