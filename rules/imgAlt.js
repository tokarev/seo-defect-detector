// Detect if any <img /> tag without alt attribute

module.exports = function (html) {
    const cheerio = require('cheerio');
    const $ = cheerio.load(html);
    let foundErrors = $('img').length - $('img[alt!=""]').length;
    if (foundErrors) {
        return `here are ${foundErrors} <img> tag without alt atribute`;
    }
    return false;
}