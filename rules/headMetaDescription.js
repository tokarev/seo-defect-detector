// Detect if header doesn’t have <meta name=“descriptions” ... /> tag

module.exports = function (html) {
    const cheerio = require('cheerio');
    const $ = cheerio.load(html);
    let foundErrors = $('head meta[name="descriptions"]').length < 1;
    if (foundErrors) {
        return `here is no <meta name="descriptions" ... /> in <head> tag`;
    }
    return false;
}