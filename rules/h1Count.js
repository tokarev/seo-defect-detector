// Detect if a HTML have more than one <H1> tag

module.exports = function (html) {
    const cheerio = require('cheerio');
    const $ = cheerio.load(html);
    let foundErrors = $("h1").length > 1;
    if (foundErrors) {
        return `here is more than one <H1> tag`;
    }
    return false;
}