// Detect if header doesn’t have <meta name=“keywords” ... /> tag

module.exports = function (html) {
    const cheerio = require('cheerio');
    const $ = cheerio.load(html);
    let foundErrors = $('head meta[name="keywords"]').length < 1;
    if (foundErrors) {
        return `here is no <meta name="keywords" ... /> in <head> tag`;
    }
    return false;
}