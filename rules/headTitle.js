// Detect if header doesn’t have <title> tag

module.exports = function (html) {
    const cheerio = require('cheerio');
    const $ = cheerio.load(html);
    let foundErrors = $('head title').length < 1;
    if (foundErrors) {
        return `here is no <title> in <head> tag`;
    }
    return false;
}