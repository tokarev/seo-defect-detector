// Detect if there’re more than 15 <strong> tag in HTML 

module.exports = function (html, config = {}) {
    let allowedCount = (config && typeof config.strongCount != 'undefined')?config.strongCount:15;
    const cheerio = require('cheerio');
    const $ = cheerio.load(html);
    let foundErrors = $('strong').length > allowedCount;
    if (foundErrors) {
        return `here is more than ${allowedCount} <strong> tag`;
    }
    return false;
}